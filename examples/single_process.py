"""
Example to setup Broker and a Worker in a single process, using dummy Cache & Queue.

Everything is in memory here.
"""
import asyncio
import logging

from aio_task import Broker, Worker


async def addition(a, b):
    await asyncio.sleep(0.5)
    return a + b


async def setup_worker():
    worker = await Worker.create()
    worker.register_handler(addition)
    await worker.start()
    return worker


async def setup_broker():
    return await Broker.create()


async def example():
    # setup broker/worker. Setup order as no importance.
    broker = await setup_broker()
    worker = await setup_worker()
    # start to produce tasks
    task_id = await broker.create_task("addition", {"a": 1, "b": 2})
    # task in progress...
    task = await broker.get_task(task_id)
    print(task)
    await asyncio.sleep(1)
    # task is over
    task = await broker.get_task(task_id)
    print(task)
    # gracefull shutdown
    await worker.close()
    await broker.close()


def main():
    logging.basicConfig(level="INFO")
    loop = asyncio.get_event_loop()
    loop.run_until_complete(example())
    loop.close()


if __name__ == '__main__':
    main()
