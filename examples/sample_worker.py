import asyncio
import logging

from aio_task import Worker


async def addition(a, b):
    return a + b


async def long_time_addition(a, b):
    await asyncio.sleep(5)
    return a + b


async def errored_task():
    raise ValueError("Oops")


async def start_worker():
    rabbitmq_config = {"url": "amqp://guest:guest@localhost:5672",
                       "routing_key": "tasks_queue"}
    redis_config = {"address": "redis://localhost"}
    worker = await Worker.create("rabbitmq", rabbitmq_config,
                                 "redis", redis_config)
    worker.register_handler(addition)
    worker.register_handler(long_time_addition, task_name="addition2")
    worker.register_handler(errored_task)
    await worker.start()
    return worker


def main():
    logging.basicConfig(level="INFO")

    loop = asyncio.get_event_loop()
    worker = loop.run_until_complete(start_worker())

    try:
        loop.run_forever()
    except KeyboardInterrupt:
        loop.run_until_complete(worker.close())

    loop.close()


if __name__ == '__main__':
    main()
