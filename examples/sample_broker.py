import asyncio
import logging

from aio_task import Broker


async def setup_broker():
    rabbitmq_config = {"url": "amqp://guest:guest@localhost:5672",
                       "routing_key": "tasks_queue"}
    redis_config = {"address": "redis://localhost"}
    return await Broker.create("rabbitmq", rabbitmq_config,
                               "redis", redis_config)


async def sample_addition(broker):
    print("make sample addition")
    task_id = await broker.create_task("addition", {"a": 1, "b": 2})
    print("wait...")
    await asyncio.sleep(0.1)
    task = await broker.get_task(task_id)
    print(task)


async def sample_addition2(broker):
    print("make long addition")
    task_id = await broker.create_task("addition2", {"a": 1, "b": 2})
    for _ in range(5):
        await asyncio.sleep(1)
        task = await broker.get_task(task_id)
        print(task)


async def hundreds_of_additions(broker):
    print("produce hundreds of additions")
    tasks_id = await asyncio.gather(*[broker.create_task("addition",
                                                         {"a": 1, "b": 2})
                                      for _ in range(100)])
    await asyncio.sleep(0.1)
    tasks = await asyncio.gather(*[broker.get_task(task_id)
                                   for task_id in tasks_id])
    print("all tasks done:", all([task.done for task in tasks]))


async def errored_task(broker):
    print("test exception in task")
    task_id = await broker.create_task("errored_task")
    await asyncio.sleep(0.1)
    task = await broker.get_task(task_id)
    print(task)


def main():
    logging.basicConfig(level="INFO")

    loop = asyncio.get_event_loop()
    broker = loop.run_until_complete(setup_broker())
    loop.run_until_complete(
        sample_addition(broker)
    )
    loop.run_until_complete(broker.close())


if __name__ == '__main__':
    main()
