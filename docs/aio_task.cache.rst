Cache API
=========

.. automodule:: aio_task.cache
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

aio\_task.cache.base module
---------------------------

.. automodule:: aio_task.cache.base
   :members:
   :undoc-members:
   :show-inheritance:

aio\_task.cache.dummy module
----------------------------

.. automodule:: aio_task.cache.dummy
   :members:
   :undoc-members:
   :show-inheritance:

aio\_task.cache.redis module
----------------------------

.. automodule:: aio_task.cache.redis
   :members:
   :undoc-members:
   :show-inheritance:

aio\_task.cache.sentinel module
-------------------------------

.. automodule:: aio_task.cache.sentinel
   :members:
   :undoc-members:
   :show-inheritance:
