Queue API
=========

.. automodule:: aio_task.queue
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

aio\_task.queue.base module
---------------------------

.. automodule:: aio_task.queue.base
   :members:
   :undoc-members:
   :show-inheritance:

aio\_task.queue.dummy module
----------------------------

.. automodule:: aio_task.queue.dummy
   :members:
   :undoc-members:
   :show-inheritance:

aio\_task.queue.rabbitmq module
-------------------------------

.. automodule:: aio_task.queue.rabbitmq
   :members:
   :undoc-members:
   :show-inheritance:

